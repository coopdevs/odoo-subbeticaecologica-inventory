odoo12-addon-account-banking-mandate==12.0.2.0.3.99.dev4
odoo12-addon-account-banking-pain-base==12.0.1.0.4.99.dev6
odoo12-addon-account-banking-sepa-credit-transfer==12.0.1.0.0.99.dev19
odoo12-addon-account-banking-sepa-direct-debit==12.0.1.3.0.99.dev8
odoo12-addon-account-due-list==12.0.1.0.0.99.dev11
odoo12-addon-account-financial-report==12.0.1.5.0
odoo12-addon-account-fiscal-year==12.0.1.0.0.99.dev25
odoo12-addon-account-invoice-supplier-self-invoice==12.0.1.1.0.99.dev1
odoo12-addon-account-payment-mode==12.0.1.0.1.99.dev16
odoo12-addon-account-payment-order==12.0.2.0.0.99.dev9
odoo12-addon-account-payment-partner==12.0.1.0.2.99.dev14
odoo12-addon-account-payment-return==12.0.2.1.1.99.dev1
odoo12-addon-base-bank-from-iban==12.0.1.0.0.99.dev4
odoo12-addon-base-technical-features==12.0.1.1.0.99.dev7
odoo12-addon-contract==12.0.8.3.10.99.dev4
odoo12-addon-contract-sale==12.0.3.0.0.99.dev1
odoo12-addon-easy-my-coop==12.0.3.3.1.99.dev12
odoo12-addon-easy-my-coop-es==12.0.0.0.16.99.dev9
odoo12-addon-l10n-es-account-bank-statement-import-n43==12.0.1.0.6.99.dev2
odoo12-addon-l10n-es-account-invoice-sequence==12.0.1.0.2.99.dev6
odoo12-addon-l10n-es-aeat==12.0.2.3.0.99.dev6
odoo12-addon-l10n-es-aeat-mod111==12.0.1.4.0.99.dev1
odoo12-addon-l10n-es-aeat-mod115==12.0.1.4.0.99.dev3
odoo12-addon-l10n-es-aeat-mod303==12.0.3.2.0
odoo12-addon-l10n-es-aeat-mod347==12.0.2.0.0.99.dev2 
odoo12-addon-l10n-es-aeat-mod349==12.0.1.3.4
odoo12-addon-l10n-es-mis-report==12.0.1.2.0.99.dev1
odoo12-addon-l10n-es-partner==12.0.1.0.3
odoo12-addon-l10n-es-toponyms==12.0.1.0.0.99.dev9
odoo12-addon-mass-editing==12.0.2.2.2
odoo12-addon-mass-mailing-list-dynamic==12.0.1.0.4.99.dev1
odoo12-addon-mass-mailing-partner==12.0.1.0.9
odoo12-addon-mis-builder==12.0.3.7.3.99.dev1
odoo12-addon-mis-builder-budget==12.0.3.5.0.99.dev9
odoo12-addon-mis-builder-cash-flow==12.0.1.3.0.99.dev11
odoo12-addon-pos-accented-search==12.0.1.0.1.99.dev1
odoo12-addon-pos-mail-receipt==12.0.1.0.0.99.dev14
odoo12-addon-pos-order-mgmt==12.0.1.1.4.99.dev4
odoo12-addon-web-decimal-numpad-dot==12.0.1.0.0.99.dev11
odoo12-addon-web-environment-ribbon==12.0.1.0.0.99.dev18
odoo12-addon-web-no-bubble==12.0.1.0.0.99.dev10
odoo12-addon-web-responsive==12.0.2.4.0.99.dev7
odoo12-addon-web-searchbar-full-width==12.0.1.0.0.99.dev8
odoo12-addon-website-sale-hide-price==12.0.1.1.0.99.dev3
odoo12-addon-website-sale-checkout-skip-payment==12.0.1.1.0.99.dev10
